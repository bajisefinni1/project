/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Benjamin
 */
public class ProgrammableThermostat{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("\t\t\t PROGRAMMABLE THERMOSTAT\n");

        // obj created to rad json parser     
        JSONParser parser = new JSONParser();
        
        TempReadWriter tmpRW = new TempReadWriter();

        // try catch class will read json file and display to user
        try{
            Object obj = parser.parse(new FileReader("temperature.json"));

            JSONObject jsonObject = (JSONObject) obj;

            Thermostat therm;
            therm = new Thermostat();
            

            // values read from Json file and then read
            therm.SetNameID((String) jsonObject.get("identifier"));
            therm.SetName((String) jsonObject.get("name"));
            therm.SetTime((String) jsonObject.get("utcTime"));
            therm.SetThermostatTemp(79);
            
            System.out.println("DATA PRINTED DIRECTLY TO THE CONSOLE");
            System.out.println(therm.ToString());

            /**
             * tmpRW.write("ID: " + (String) jsonObject.get("identifier") + "Name: " 
                    + (String) jsonObject.get("name") + "UTC time: "   
                    + (String) jsonObject.get("utcTime")+ "\n" +
                    "Temperature: " + Integer.toString(therm.GetThermostatTemp()));
             */
            
            tmpRW.write(therm.ToString());
            
              
            
           
            

        }catch (FileNotFoundException e) {
        } catch (IOException e) {
        } catch (ParseException e) {
        }
        
        
        tmpRW.read();

        
        

        }// end main

 

   
    }// end class
    

