/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

import java.io.*;
import java.util.Scanner;

/**
 *
 * @author Benjamin
 */
public class TempReadWriter {
 

    public TempReadWriter() {
       
    }
    
    
    // read information from file
    public void read(){
        
        System.out.println();
        System.out.println("DATA READ FROM FILE WITH SAVED INFORMATION");
        
        String fileName = "temperatureValues.txt";
        
        
        try {
            // FileReader reads text files in the default encoding.
            File file = 
                 new File(fileName); 
            Scanner sc = new Scanner(file); 
  
            while (sc.hasNextLine()){
                System.out.println(sc.nextLine()); 
            } 
                       
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
        }
    }// end read method
    
      
    // write information to file
    public void write(String data) throws IOException{
    
        File file = new File("temperatureValues.txt");
        PrintWriter writer = new PrintWriter(new FileWriter(file, false));
        writer.println(data + "\n");
        writer.println();
        
        writer.close();
        
    }
    
    
    
    
    
    
}// end class
